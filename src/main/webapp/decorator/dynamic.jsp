<%@ page pageEncoding='utf-8' %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>

<decorator:usePage id="sitemeshPage"/>

<html>

<head>
<title>Decorated, dynamically - <decorator:title/></title>
<link rel='stylesheet' type='text/css' href='/style.css'>
<decorator:head/>
</head>
<body>
<h2>The decorated page</h2>
<section>
<decorator:body/>
</section>

<h2>More details</h2>

<h3>A property, resolved against the action</h3>
<p>If the action defined it: currentTime = <s:property value='currentTime'/>.</p>

<h3>A value taken from the original jsp's <code>&lt;meta></code> with <code>s:property</code></h3>
<p><s:property value='#attr.sitemeshPage.getProperty("meta.hint")' default='(unset)' />

<h3>...and with <code>decorator:getProperty</code></h3>
<p><decorator:getProperty property='meta.hint'/></p>

<h3>Invoking a Java method to get details via <code>PageContext</code></h3>
<p>Details are: <%= dev.jw.strutssitemeshsample.util.DetailsFromSitemesh.details() %>
</p>

<h3>Transforming data from <code>&lt;meta></code> with code</h3>
<p>Results are:</p>
<ol>
<s:iterator value="getLines('specified-in-decorator')">
<li>Value is <s:property/></li>
</s:iterator>
</ol>
</body>
</html>
