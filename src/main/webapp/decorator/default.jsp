<%@ page pageEncoding='utf-8' %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>

<html>

<head>
<title>Decorated - <decorator:title/></title>
<link rel='stylesheet' type='text/css' href='/style.css'>
<decorator:head/>
</head>
<body>
<decorator:body/>
<hr>
</p>
</body>
</html>
