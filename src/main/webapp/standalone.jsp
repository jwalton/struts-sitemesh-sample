<%@ page pageEncoding="utf-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
<body>
<h1>A regular .jsp using Struts tags</h1>
<p><code>s:property</code> will evaluate expressions: <s:property value="1 + 2"/>.</p>
<h1>s:text doesn't currently know anything</h1>
<p>Text is: <s:text name='specific.message'/></p>
<p>HTML is: <s:text name='specific.message.html'/></p>

<h1>Using s:i18n to load messages</h1>
<s:i18n name='basic'>

<h2>s:text now knows those values</h2>
<p>Text is: <s:text name='specific.message'/></p>
<p>HTML is: <s:text name='specific.message.html'/></p>

</s:i18n>

</body>
</html>
