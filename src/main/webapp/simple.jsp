<%@ page pageEncoding="utf-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
<body>
<h1>The result of an action</h1>
<p>The time is provided by the action: <s:property value="currentTime"/>.</p>
<p>An unknown property provides an empty string: <s:property value="unknownProperty"/>.</p>
<p>An i18n string defined for this action is available: <s:text name='specific.message'/></p>
</body>
</html>
