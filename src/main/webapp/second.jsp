<%@ page pageEncoding="utf-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
<body>
<h1>The result of an action with multiple methods</h1>
<p>The invoked method name was: <s:property value="invokedMethodName"/>.</p>
</body>
</html>
