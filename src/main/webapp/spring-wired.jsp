<%@ page pageEncoding="utf-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
<body>
<h1>The result of a Spring wired action (<s:property value="class.simpleName"/>)</h1>
<p>The message is provided by the action: <s:property value="message"/></p>
<p>Some additional things are provided: <s:property value="things"/></p>
</body>
</html>
