<%@ page pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
<body>
<h1>A regular .jsp using Struts <code>action</code> tags</h1>

<h2>When assigning to a variable</h2>
<s:action namespace='/' name='simple' var='result'>
The action is not available in the body of the tag: <s:property value='currentTime'/>.
</s:action>

<h2>Afterwards</h2>
<p>The <code>currentTime</code> property is not defined: <s:property value='currentTime'/>.</p>
<p>But <code>#result.currentTime</code> is: <s:property value='#result.currentTime'/>.</p>

</body>
</html>
