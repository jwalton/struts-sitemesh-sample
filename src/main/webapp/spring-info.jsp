<%@ page pageEncoding="utf-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<title>Spring information</title>
</head>
<body>
<h1>The result of an action</h1>

<p>(<s:property value='toString()'/>)</p>

<h3>Properties wired by Struts</h3>
<p>Spring was configured: <s:property value="injectedStartMessage"/>.</p>
<p>Things are: <s:property value="things"/>.</p>

<h3>Properties retrieved directly from the ApplicationContext</h3>
<p>Spring was configured: <s:property value="messageFromApplicationContext"/>.</p>
<p>Spring was configured: <s:property value="dateFromApplicationContext"/>.</p>

<h3>Properties provided by the action</h3>
<p>The current time is provided by the action: <s:property value="currentTime"/>.</p>
<p>(Try to <a href='/reload.action'>reload again</a>.)</p>
</body>
</html>
