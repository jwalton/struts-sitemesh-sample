<%@ page pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
<body>

<h1>Custom error page - 404</h1>
<p>Struts tags are available: <s:property value='1 + 2'/>.</p>

<s:i18n name='basic'>

<p>Text is: <s:text name='specific.message'/></p>
<p>HTML is: <s:text name='specific.message.html'/></p>
</s:i18n>

</body>
</html>
