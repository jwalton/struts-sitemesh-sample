package dev.jw.strutssitemeshsample;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.ConfigurableWebApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.opensymphony.xwork2.Action;

import dev.jw.strutssitemeshsample.util.SingletonObjectFactory;

public class ReconfigureSpring implements Action
{
    private int index;

    public void setConfigurationIndex(int index)
    {
        this.index = index;
    }

    @Override
    public String execute() throws Exception
    {
        if (index < 1 || index > 2)
        {
            throw new IllegalArgumentException("Bad override index: " + index);
        }

        WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());

        ((ConfigurableWebApplicationContext) wac).setConfigLocations(new String[]{
                "/WEB-INF/applicationContext.xml",
                "/WEB-INF/applicationContext-override-" + index + ".xml"
            });

        ((ConfigurableWebApplicationContext) wac).refresh();

        SingletonObjectFactory.getObjectFactory().setApplicationContext(wac);

        return SUCCESS;
    }
}
