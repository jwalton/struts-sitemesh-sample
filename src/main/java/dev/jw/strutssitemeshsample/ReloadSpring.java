package dev.jw.strutssitemeshsample;

import org.apache.struts2.ServletActionContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.opensymphony.xwork2.ActionSupport;

import dev.jw.strutssitemeshsample.util.SingletonObjectFactory;

public class ReloadSpring extends ActionSupport
{
    @Override
    public String execute() throws Exception
    {
        WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());

        ((ConfigurableApplicationContext) wac).refresh();

        SingletonObjectFactory.getObjectFactory().setApplicationContext(wac);

        return SUCCESS;
    }
}
