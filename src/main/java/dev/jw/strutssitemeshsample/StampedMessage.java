package dev.jw.strutssitemeshsample;

import java.util.Date;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class StampedMessage implements InitializingBean, DisposableBean
{
    private final Date stamp;
    private final String message;

    public StampedMessage(Date stamp, String message)
    {
        this.stamp = stamp;
        this.message = message;
    }

    public String toString()
    {
        return stamp + ": " + message;
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        System.err.println("afterPropertiesSet: " + this);
    }

    @Override
    public void destroy() throws Exception
    {
        System.err.println("destroy: " + this);
    }
}
