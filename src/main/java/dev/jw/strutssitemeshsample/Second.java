package dev.jw.strutssitemeshsample;

import com.opensymphony.xwork2.ActionSupport;

public class Second extends ActionSupport
{
    private String result = "(unexecuted)";

    public String execute()
    {
        result = "execute()";
        return SUCCESS;
    }

    public String doSomethingElse()
    {
        result = "doSomethingElse()";
        return SUCCESS;
    }

    public String notAllowed()
    {
        result = "notAllowed()";
        return SUCCESS;
    }

    public String getInvokedMethodName()
    {
        return result;
    }

    public String doMethodA()
    {
        result = "doMethodA()";
        return SUCCESS;
    }

    public String doMethodB()
    {
        result = "doMethodB()";
        return SUCCESS;
    }

    public String methodC()
    {
        result = "methodC()";
        return SUCCESS;
    }

    public String methodD()
    {
        result = "methodD()";
        return SUCCESS;
    }
}
