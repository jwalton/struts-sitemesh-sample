package dev.jw.strutssitemeshsample;

import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.opensymphony.xwork2.Action;

public class SpringInfo implements Action
{
    private StampedMessage message;
    private List<String> things;

    private StampedMessage messageFromApplicationContext;
    private Date dateFromApplicationContext;

    private Date now;

    public void setMessage(StampedMessage message)
    {
        this.message = message;
    }

    public void setThings(List<String> things)
    {
        this.things = things;
    }

    @Override
    public String execute() throws Exception
    {
        WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());

        this.messageFromApplicationContext = wac.getBean(StampedMessage.class);
        this.dateFromApplicationContext = wac.getBean(Date.class);

        this.now = new Date();
        return SUCCESS;
    }

    public String getInjectedStartMessage()
    {
        return message.toString();
    }

    public List<String> getThings()
    {
        return things;
    }

    public String getMessageFromApplicationContext()
    {
        return messageFromApplicationContext.toString();
    }

    public String getDateFromApplicationContext()
    {
        return dateFromApplicationContext.toString();
    }

    public String getCurrentTime()
    {
        return now.toString();
    }
}