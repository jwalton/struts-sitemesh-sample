package dev.jw.strutssitemeshsample;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

public class SpringConstructorInjected extends ActionSupport
{
    private final StampedMessage message;
    private final List<String> things;

    public SpringConstructorInjected(StampedMessage message, List<String> things)
    {
        this.message = message;
        this.things = things;
    }

    @Override
    public String execute() throws Exception
    {
        if (message == null || things == null)
        {
            throw new IllegalStateException("Resources have not been provided: " + message + ", " + things);
        }

        return SUCCESS;
    }

    public StampedMessage getMessage()
    {
        return message;
    }

    public List<String> getThings()
    {
        return things;
    }

    public String getCurrentTime()
    {
        return "now-ish";
    }
}
