package dev.jw.strutssitemeshsample;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

public class SpringSetterInjected extends ActionSupport
{
    private StampedMessage message;
    private List<String> things;

    public void setMessage(StampedMessage msg)
    {
        this.message = msg;
    }

    public void setThings(List<String> things)
    {
        this.things = things;
    }

    @Override
    public String execute() throws Exception
    {
        if (message == null || things == null)
        {
            throw new IllegalStateException("Resources have not been provided: " + message + ", " + things);
        }

        return SUCCESS;
    }

    public StampedMessage getMessage()
    {
        return message;
    }

    public List<String> getThings()
    {
        return things;
    }

    public String getCurrentTime()
    {
        return "now-ish";
    }
}
