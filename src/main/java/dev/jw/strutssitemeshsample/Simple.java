package dev.jw.strutssitemeshsample;

import java.util.Date;

import com.opensymphony.xwork2.ActionSupport;

public class Simple extends ActionSupport
{
    /**
     * Does nothing, successfully.
     */
    @Override
    public String execute() throws Exception
    {
        return SUCCESS;
    }

    public String getCurrentTime()
    {
        return new Date().toString();
    }
}
