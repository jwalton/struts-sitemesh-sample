package dev.jw.strutssitemeshsample;

import java.util.Date;

import com.opensymphony.xwork2.ActionSupport;

import dev.jw.strutssitemeshsample.util.DetailsFromSitemesh;

/**
 * Doesn't do much, but intended to be decorated by SiteMesh with dynamic content.
 */
public class SimpleDecorated extends ActionSupport
{
    /**
     * Does nothing, successfully.
     */
    @Override
    public String execute() throws Exception
    {
        return SUCCESS;
    }

    public String getCurrentTime()
    {
        return new Date().toString();
    }

    public Iterable<String> getLines(String data)
    {
        return DetailsFromSitemesh.getLines(data);
    }
}
