package dev.jw.strutssitemeshsample.util;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.jsp.PageContext;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.module.sitemesh.Page;

public class DetailsFromSitemesh
{
    public static String details()
    {
        PageContext pageContext = ServletActionContext.getPageContext();
        if (pageContext != null)
        {
            // get the sitemesh page out of the page context
            final Page page = (Page) pageContext.getAttribute("sitemeshPage");
            if (page != null)
            {
                return "Properties are: " + page.getProperties();
            }
            else
            {
                return "(no sitemeshPage found in PageContext)";
            }
        }
        else
        {
            return "(no PageContext found)";
        }
    }

    public static Iterable<String> getLines(String data)
    {
        PageContext pageContext = ServletActionContext.getPageContext();
        if (pageContext != null)
        {
            // get the sitemesh page out of the page context
            final Page page = (Page) pageContext.getAttribute("sitemeshPage");
            if (page != null)
            {
                String prefix = page.getProperty("meta.prefix");

                Collection<String> coll = new ArrayList<String>();

                for (int i = 0; i < 10; i++)
                {
                    coll.add(prefix + i + " " + data);
                }

                return coll;
            }
        }

        return null;
    }
}
