package dev.jw.strutssitemeshsample.util;

import org.apache.struts2.result.ServletDispatcherResult;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.config.Configuration;
import com.opensymphony.xwork2.config.ConfigurationException;
import com.opensymphony.xwork2.config.PackageProvider;
import com.opensymphony.xwork2.config.entities.ActionConfig;
import com.opensymphony.xwork2.config.entities.PackageConfig;
import com.opensymphony.xwork2.config.entities.PackageConfig.Builder;
import com.opensymphony.xwork2.config.entities.ResultConfig;

public class CustomConfiguration implements PackageProvider
{
    @Override
    public void init(Configuration configuration) throws ConfigurationException
    {
        Builder b = new PackageConfig.Builder("custom");
        b.namespace("/namespace");

        ActionConfig.Builder action = new ActionConfig.Builder("custom", "action1", CustomAction.class.getName());

        com.opensymphony.xwork2.config.entities.ResultConfig.Builder resultConfig = new ResultConfig.Builder(Action.SUCCESS, ServletDispatcherResult.class.getName());
        resultConfig.addParam("location", "/simple.jsp");

        action.addResultConfig(resultConfig.build());

        b.addActionConfig("action1", action.build());

        configuration.addPackageConfig("custom", b.build());
    }

    @Override
    public void loadPackages() throws ConfigurationException
    {
    }

    @Override
    public boolean needsReload()
    {
        return false;
    }

    public static class CustomAction implements Action
    {
        @Override
        public String execute() throws Exception
        {
            return SUCCESS;
        }

        public String getCurrentTime()
        {
            return "(Not really the current time; provided by " + this + ")";
        }
    }
}
