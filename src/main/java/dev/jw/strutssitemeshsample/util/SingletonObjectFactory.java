package dev.jw.strutssitemeshsample.util;

import javax.servlet.ServletContext;

import org.apache.struts2.StrutsConstants;
import org.apache.struts2.spring.StrutsSpringObjectFactory;

import com.opensymphony.xwork2.inject.Container;
import com.opensymphony.xwork2.inject.Inject;
import com.opensymphony.xwork2.spring.SpringObjectFactory;

public class SingletonObjectFactory extends StrutsSpringObjectFactory
{
    private static SingletonObjectFactory instance = null;

    @Inject
    public SingletonObjectFactory(
            @Inject(value=StrutsConstants.STRUTS_OBJECTFACTORY_SPRING_AUTOWIRE,required=false) String autoWire,
            @Inject(value=StrutsConstants.STRUTS_OBJECTFACTORY_SPRING_AUTOWIRE_ALWAYS_RESPECT,required=false) String alwaysAutoWire,
            @Inject(value=StrutsConstants.STRUTS_OBJECTFACTORY_SPRING_USE_CLASS_CACHE,required=false) String useClassCacheStr,
            @Inject(value=StrutsConstants.STRUTS_OBJECTFACTORY_SPRING_ENABLE_AOP_SUPPORT,required=false) String enableAopSupport,
            @Inject ServletContext servletContext,
            @Inject(StrutsConstants.STRUTS_DEVMODE) String devMode,
            @Inject Container container)
    {
        super(autoWire, alwaysAutoWire, useClassCacheStr, enableAopSupport,
                servletContext, devMode, container);

        if (instance != null)
        {
            throw new IllegalStateException("Only one instance allowed");
        }

        instance = this;
    }

    public static SpringObjectFactory getObjectFactory()
    {
        if (instance == null)
        {
            throw new IllegalStateException("No instance known");
        }

        return instance;
    }
}
